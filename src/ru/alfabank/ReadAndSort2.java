package ru.alfabank;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class ReadAndSort2 {
    public static void main(String[] args) throws IOException {
//        Генерируем файл
        GenerateFile gen = new GenerateFile();
        gen.generateIntFile();
//         Через TreeSet
        TreeSet<Integer> map = new TreeSet<>();
//        Читаем файл
        Scanner scanner = new Scanner(new File("intFile.txt"));
//        Конверт в int и добавление в TreeSet
        for (String str: scanner.next().split(",")){
            map.add(Integer.parseInt(str));
        }
        System.out.println(map.toString());
        System.out.println(map.descendingSet().toString());




//        Через Bubble sort
        scanner = new Scanner(new File("intFile.txt"));

//        Convert to int[]
        String[] strings = scanner.next().split(",");
        int[] listInt = new int[strings.length];
        for (int i = 0; i < strings.length; i++) {
            listInt[i] = Integer.parseInt(strings[i]);
        }

//        BubleSort Asc
        int numb = listInt.length;
        for (int i = 0; i < numb; i++) {
            for (int j = 1; j < (numb - i); j++) {
                if (listInt[j - 1] > listInt[j]) {
                    int temp = listInt[j - 1];
                    listInt[j - 1] = listInt[j];
                    listInt[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(listInt));

//        BubbleSort Desc
        for(int i = 0; i < numb - 1; i++){
            for(int j = 0; j < numb -1; j++){
                if(listInt[j] < listInt[j+1]){
                    int temp = listInt[j];
                    listInt[j] = listInt[j+1];
                    listInt[j+1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(listInt));

    }
}
