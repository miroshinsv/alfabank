package ru.alfabank;

import java.math.BigInteger;

public class Factorial {
    public static void main(String[] args) {
        System.out.println(factorial(20));
    }

    static long factorial(int n){
        return (n<=0) ? 1: n * factorial(n -1);
   }
}
