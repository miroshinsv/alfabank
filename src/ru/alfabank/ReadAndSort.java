package ru.alfabank;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ReadAndSort {
    public static void main(String[] args) throws IOException {
        GenerateFile gen = new GenerateFile();
        gen.generateIntFile();


//        Читаем файл сначала в строки
        String[] strings = new BufferedReader(new FileReader("intFile.txt")).readLine().split(",");
//        ArrayList<Integer> arrayList = new ArrayList<>();
        int max = -1;

        for (String str : strings){
            int tempInt = Integer.parseInt(str);
            if (tempInt > max){
                max = tempInt;
                System.out.print(tempInt + " ");
//                arrayList.add(tempInt);
            }

            if(max == 20)break;
        }
        System.out.println();
//        System.out.println(arrayList.toString());

        int min = 21;
        for (String str: strings){
            int tempInt = Integer.parseInt(str);
            if (tempInt < min){
                min = tempInt;
                System.out.print(tempInt + " ");
            }
            if (min == 0)break;
        }
    }
}
