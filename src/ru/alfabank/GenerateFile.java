package ru.alfabank;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

class GenerateFile {
     void generateIntFile() throws IOException {
        Random random = new Random();
        ArrayList<Integer> list = new ArrayList<>();

//        Создаем объект FileWriter для записи в файл
        FileWriter fileWriter = new FileWriter("intFile.txt");

//        Заполняем случаными числами от 1 до 20
        while (list.size() <21){
            int tempInt = random.nextInt(21);
//            Проверяем на уникальность. Если числа нет в списке, то записываем в файл
            if(!list.contains(tempInt)){
//                Добавляем в список уже существующих чисел
                if (list.size()!=0){
                    fileWriter.write(",");
                }
                list.add(tempInt);
//                Записываем в файл
                fileWriter.write(String.valueOf(tempInt));
            }
        }
        fileWriter.close();
    }
}
